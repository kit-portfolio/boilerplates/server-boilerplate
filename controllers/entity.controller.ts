import {Request, Response} from 'express';
import Entity from '../models/Entity.model';
import message, {ENTITY, OPERATION} from '../config/messages';

/**
 * Create new entity
 * @route   POST /entities
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 * @returns void
 */
export function createEntity(rq: Request, rs: Response): void {
    Entity.create(rq.body)
        .then((entity) => rs.status(201).json(entity))
        .catch((e) =>
            rs.status(400).json({
                message: message.entityManipulation.error(ENTITY.ENTITY, OPERATION.CREATE),
                error: e,
            }),
        );
}

/**
 * Update entity by ID
 * @route   PUT /entities/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function updateEntity(rq: Request, rs: Response): void {
    Entity.findOne({_id: rq.params.id})
        .then((entity) => {
            if (!entity) {
                rs.status(404).json({
                    message: message.errors.entitySpecific.notFound(ENTITY.ENTITY),
                });
                return;
            } else {
                Entity.findOneAndUpdate(
                    {_id: rq.params.id},
                    {$set: rq.body},
                    {new: true},
                )
                    .then((entity) => rs.status(200).json(entity))
                    .catch((e) =>
                        rs.status(400).json({
                            message: message.entityManipulation.error(ENTITY.ENTITY, OPERATION.UPDATE),
                            error: e,
                        }),
                    );
            }
        })
        .catch((e) =>
            rs.status(400).json({
                message: message.errors.generic.serverError,
                error: e,
            }),
        );
}

/**
 * Get all entities
 * @route   GET /entities
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getEntities(rq: Request, rs: Response): void {
    Entity.find()
        .then((entity) => rs.status(200).json(entity))
        .catch((e) =>
            rs.status(400).json({
                message: message.errors.generic.serverError,
                error: e,
            }),
        );
}

/**
 * Get entity by ID
 * @route   GET /entities/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getEntityById(rq: Request, rs: Response): void {
    Entity.findOne({_id: rq.params.id})
        .then((entity) => {
            if (!entity) rs.status(404).json({message: message.errors.entitySpecific.notFound(ENTITY.ENTITY)})
            else rs.status(200).json(entity)
        })
        .catch((e) =>
            rs.status(400).json({
                message: message.errors.generic.serverError,
                error: e,
            }),
        );
}

/**
 * Delete entity by ID
 * @route   DELETE /entities/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function deleteEntity(rq: Request, rs: Response): void {
    Entity.findOne({_id: rq.params.id}).then((entity) => {
        if (!entity) {
            return rs.status(404).json({
                message: message.errors.entitySpecific.notFound(ENTITY.ENTITY),
            });
        } else {
            Entity.deleteOne({_id: rq.params.id})
                .then(() =>
                    rs.status(204).json({
                        message: message.entityManipulation.success(ENTITY.ENTITY, OPERATION.DELETE),
                    }),
                )
                .catch((e) =>
                    rs.status(400).json({
                        message: message.errors.generic.serverError,
                        error: e,
                    }),
                );
        }
    });
}
