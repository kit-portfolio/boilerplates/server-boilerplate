export enum MODEL {
 ENTITY = 'entity',
}

export enum URN {
 ENTITY = '/api/entities',
 INDEX = '/',
}
