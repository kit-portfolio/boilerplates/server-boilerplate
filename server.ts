// MODULES
import express, {Application} from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import path from 'path';
import cors from 'cors';
import message from './config/messages';
import keys from './config/keys';
import {URN} from './config/constants';

// ROUTES
import * as router from './routes/root.router';

// APPLICATION CONFIG
const app: Application = express();
const port: string = process.env.PORT || '5000';
const db: string = keys.mongoURI;

// MIDDLEWARE
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

// Connect to MongoDB
mongoose
    .connect(db, {useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
    .then(() => console.log(message.system.databaseConnected))
    .catch((e) => console.log(e));

// APPLICATION ROUTES
app.use(URN.ENTITY, router.entity);
app.use(URN.INDEX, router.index);

// Server static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

app.listen(port, () => console.log(message.system.serverLaunched(port)));
