import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

/**
 * Interface is required to let Typescript know the shape of document object.
 * This allows you to use model content props in controllers
 * @hint
 */
export interface IEntity extends Document {
    lorem: string,
    ipsum?: string,
    dolor: TYPE,
    sit: number,
    amet: number,
    consectetur: boolean,
    adipiscing: string,
}

enum TYPE {
    FANCY = 'Fancy type',
    SHINY = 'Shiny type',
}

const EntitySchema: Schema = new Schema(
    {
        lorem: {
            type: String,
            required: true,
            unique: true,
        },
        ipsum: {
            type: String,
            default: 'Anonymous',
        },
        dolor: {
            type: String,
            required: true,
            enum: [TYPE.FANCY, TYPE.SHINY],
        },
        sit: {
            type: Number,
            validate: /^\d{10}$/,
            required: [true, 'Why no sit?'],
        },
        amet: {
            type: Number,
            required: true,
            min: [6, 'Too few amet'],
            max: 12,
        },
        consectetur: {
            type: Boolean,
            required: true,
        },
        adipiscing: {
            type: Date,
            default: Date.now,
        },
    },
    {strict: false},
);

export default model<IEntity>(MODEL.ENTITY, EntitySchema);
